
public class Fibonacci {
    /**
     * Method prints the even numbers from the interval that user inputted
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    public void printEvenNumbers(int start, int end) {
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }
    }
    /**
     * Method prints the odd numbers from the interval that user inputted
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    public void printOddNumbers(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }
    /**
     * Method prints sum of the even numbers
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    public void printSumEvenNumber(int start, int end) {
        int sum = 0;
        for (int i = end; i >= start; i--) {
            if (i % 2 == 0) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
    /**
     * Method prints sum of the odd numbers
     *
     * @param start start of the interval
     * @param end   end of the interval
     */
    public void printSumOddNumber(int start, int end) {
        int sum = 0;
        for (int i = end; i >= start; i--) {
            if (i % 2 == 1) {
                sum += i;
            }
        }
        System.out.println(sum);
    }
    /**
     * Method find the Fibonacci numbers and print the biggest odd and even numbers from Fibonacci collection.
     *
     *
     * @param size of Fibonacci numbers that user inputted
     */
    public void fibonacciNumbers(int size) {
        int F1 = 0;
        int F2 = 0;
        int[] fibaNumbers = new int[size];
        fibaNumbers[0] = 0;
        fibaNumbers[1] = 1;
        for (int i = 0; i < size - 2; i++) {
            int sum = fibaNumbers[i] + fibaNumbers[i + 1];
            if (sum % 2 == 1)
                F1 = sum;
            else
                F2 = sum;
            fibaNumbers[i + 2] = sum;
        }
        System.out.println(F1 + " " + F2);
        for (int i = 0; i < size; i++) {
            System.out.println(fibaNumbers[i]);
        }
    }
    /**
     * Method find and print percentage of odd and even numbers.
     *
     *
     * @param size of Fibonacci numbers that user inputted
     */
    public void findPercentageOfNumber(int size) {
        int numberOdd = 1;
        int numberEven = 1;
        int[] fibaNumbers = new int[size];
        fibaNumbers[0] = 0;
        fibaNumbers[1] = 1;
        for (int i = 0; i < size - 2; i++) {
            int sum = fibaNumbers[i] + fibaNumbers[i + 1];
            if (sum % 2 == 1)
                numberOdd++;
            else
                numberEven++;
            fibaNumbers[i + 2] = sum;
        }
        System.out.println("Percent of odd number: " + (double)numberOdd/size*100);
        System.out.println("Percent of even number: " + (double)numberEven/size*100);
        for (int i = 0; i < size; i++) {
            System.out.println(fibaNumbers[i]);
        }
    }
}



